# README #

### The Book List ###

* A demonstration application written in ASP.NET C# MVC


### How do I get set up? ###

* git clone https://corneym@bitbucket.org/corneym/mal-asp.net-mvc.git
* Open Book.soln in Visual Studio
* Run in a browser

### Written by ###

* Malcolm Corney
* m.corney@tpg.com.au