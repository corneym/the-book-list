﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Books.Models;
using Microsoft.AspNet.Identity.Owin;

namespace Books.Controllers {
   public class HomeController : Controller {
      public ActionResult Index() {
         return View();
      }

      public ActionResult About() {
         ViewBag.Message = "";

         return View();
      }

      public ActionResult Contact() {
         ViewBag.Message = "";

         return View();
      }


        [HttpPost]
        public ActionResult Contact(string message)
        {
            // TODO: send the message to HQ
            ViewBag.TheMessage = "Thanks, we got your message!";
            return PartialView("_ContactThanks");
        }

        public ActionResult Serial(string letterCase)
        {
            var serial = "ASPNETMVC5ATM1";
            if (letterCase == "lower")
            {
                return Content(serial.ToLower());
            }
            // return new HttpStatusCodeResult(403);
            // return Json(new { name = "serial number", value = "serial" }, JsonRequestBehavior.AllowGet);
            return RedirectToAction("Index");
        }
    }
}