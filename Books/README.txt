﻿The application is a simple book list.

User name: guest@test.com
Password:  B00kl!st

Alternatively, registration is enabled.

User Authentication is enabled.  Users cannot access the book list without logging in.  If the View Books button is pressed without being logged in the user is redirected to the login page.

The main view is a list of books showing title, author and year published.  Each column in the table can be sorted ascending or descending by clicking the column header.  Authors are sorted by last name.

New book data can be entered.
Book data can be edited.

Both of these actions invoke forms validation.

Books can be deleted from the Edit form.

Data is stored in a SQL Server within the project solution.

The applciation was developed using Visual Studio 2013 and uses ASP.NET C# MVC.

Malcolm Corney
1/9/2015
m.corney@tpg.com.au